#!/usr/bin/env node

const { gitDescribeSync } = require('git-describe');
const { writeFile, ensureFile } = require('fs-extra');
const { resolve } = require("path");


async function main() {
  const argPath = process.argv.slice(2);
  let destPath = require.resolve("./index.js");
  if (argPath.length === 1) {
    destPath = resolve(argPath[0]);
  }

  const gitInfoStr = JSON.stringify(gitDescribeSync({
    dirtyMark: false,
    dirtySemver: false
  }).hash, null, 4);
  const dateStr = new Date().toUTCString();

  await ensureFile(destPath);
  await writeFile(destPath, computeBuildInfoFileContent(gitInfoStr, dateStr), { encoding: 'utf-8' } )
}

function computeBuildInfoFileContent(gitInfoStr, dateStr) {
  return `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
module.exports={
  GIT_VERSION: ${gitInfoStr},
  BUILD_DATE: "${dateStr}",
  getBuildInfoMessage: function getBuildInfoMessage(packageVersion, commitHash, buildDate) {
    return \`Package with version: \${packageVersion} built at \${buildDate} based on commit: \${commitHash} \`;
  }
};
/* tslint:enable */
`
}

main();