// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
module.exports={
  GIT_VERSION: "placeholder_git_version",
  BUILD_DATE: "placeholder_build_date",
  getBuildInfoMessage: function getBuildInfoMessage(packageVersion, commitHash, buildDate) {
    return `Package with version: ${packageVersion} built at ${buildDate} based on commit: ${commitHash} `;
  }
};
/* tslint:enable */
